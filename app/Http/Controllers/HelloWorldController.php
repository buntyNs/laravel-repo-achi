<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Service\custom\HelloWorldService;

class HelloWorldController extends Controller
{
    private $helloService;

    public function __construct(HelloWorldService $helloService)
    {
        $this->helloService = $helloService;
    }

    public function view(){
        $hello = $this->helloService->all();
        return view('hello', compact('hello'));
    }

}
